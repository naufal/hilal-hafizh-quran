package id.hilal.hafizhquran.feature.ayahlist

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import id.hilal.hafizhquran.R
import id.hilal.hafizhquran.model.Ayah
import id.hilal.hafizhquran.model.Surah
import kotlinx.android.synthetic.main.activity_ayah_list.*

class AyahListActivity : AppCompatActivity() {

    var surah: Surah = Surah("", ArrayList<Ayah>())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ayah_list)
        surah = intent.extras.getSerializable("surah") as Surah
        title = surah.name
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        ayahList.layoutManager = LinearLayoutManager(this)
        ayahList.hasFixedSize()
        ayahList.adapter = AyahListAdapter(surah.ayahs)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
