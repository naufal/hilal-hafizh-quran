package id.hilal.hafizhquran.content

import id.hilal.hafizhquran.model.Ayah
import id.hilal.hafizhquran.model.Surah

/*
 * Created by littleflower on 8/13/17.
 */

var content: ArrayList<Surah> = ArrayList<Surah>()

fun initContent() {
    // AL-Fatihah
    val fatihah  = ArrayList<Ayah>()
    fatihah.add(Ayah("بِسْمِ الِلّٰهِ الرَّحْمٰنِ الرَّحِيْمِ"))
    fatihah.add(Ayah("اَالْحَمْدُ لِلّٰهِ رَبِّ الْعٰلَمِيْنَ"))
    fatihah.add(Ayah("اَلرَّحْمٰنِ الرَّحِيْمِ"))
    fatihah.add(Ayah("مٰلِكِ يَوْمِ الدِّيْنِ"))
    fatihah.add(Ayah("اِيَّاكَ نَعْبُدُ وَاِيَّاكَ نَسْتَعِيْنُ"))
    fatihah.add(Ayah("اِهْدِنَا الصِّرَاطَ الْمُسْتَقِيْمَ"))
    fatihah.add(Ayah("صِرَاطَ الَّذِ يْنَ اَنْعَمْتَ عَلَيْهِمْ ۙ غَيْرِ ٱلْمَغْضُوْبِ عَلَيْهِمْ وَلاَ ٱلضَّآلِّيْنَ"))
    // all surah
    content.add(Surah("Al-Fatihah", fatihah))
    content.add(Surah("Al-Kafirun", fatihah))
    content.add(Surah("Al-Ikhlas", fatihah))
    content.add(Surah("Al-Falaq", fatihah))
    content.add(Surah("An-Naas", fatihah))
}