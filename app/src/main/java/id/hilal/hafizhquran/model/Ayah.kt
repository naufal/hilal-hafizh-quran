package id.hilal.hafizhquran.model

import java.io.Serializable

/*
 * Created by littleflower on 8/13/17.
 */
data class Ayah(val content: String) : Serializable {
}